package szp.rafael.example.cache;

import io.quarkus.runtime.Startup;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import szp.rafael.example.studentGrades.StudentGradesCache;
import szp.rafael.example.studentGrades.StudentGradesMockDAO;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.logging.Logger;

@Startup
@ApplicationScoped
public class CacheWarmer {

  Logger logger = Logger.getLogger(getClass().getName());

  @Inject
  StudentGradesMockDAO sgDao;

  @ConfigProperty(name = "example.cache.student-grades.ttl")
  public String ttlSeconds;

  @PostConstruct
  public void init() {
    logger.info("CACHE WARMER STARTED");
    StudentGradesCache.createTTLIndex(Long.valueOf(ttlSeconds.replaceAll("\\D","")));
    logger.info("Initializing StudentGradesCache");
    if (StudentGradesCache.count() < 1) {
      StudentGradesCache.warmCache(sgDao);
    }
  }
}
