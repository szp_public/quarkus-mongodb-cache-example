package szp.rafael.example.studentGrades;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.kohsuke.randname.RandomNameGenerator;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Singleton
public class StudentGradesMockDAO {

  @ConfigProperty(name = "example.mock.size")
  public String studentGradesMockSize;

  public List<StudentGrades> findAll() {
    List<StudentGrades> sgs = new ArrayList<>();
    RandomNameGenerator rnd = new RandomNameGenerator(0);
    LongStream.range(0, Long.valueOf(studentGradesMockSize)).forEach(i -> {
      String name = rnd.next().toUpperCase(Locale.ROOT).replaceAll("_", " ");
      StudentGrades sg = new StudentGrades(name, Long.valueOf(i), randomGrade(), randomGrade(), randomGrade());
      sgs.add(sg);
    });
    return sgs;
  }

  private float randomGrade() {
    Random random = new Random();
    float grade = Float.parseFloat(String.format("%.1f", random.nextFloat() * 10.0f).replaceAll(",", "."));
    return grade;
  }

  public List<StudentGrades> findByName(String name) {
    ArrayList<StudentGrades> result;
    if(name.equals("BYPASS DAO")){
      result = new ArrayList<>(1);
      StudentGrades sg = new StudentGrades(name, 999_999_999L, randomGrade(), randomGrade(), randomGrade());
      result.add(sg);
    }else {
       result = findAll().stream()
        .filter(s -> s.getStudentName()
          .contains(name.toUpperCase(Locale.ROOT)))
        .collect(Collectors.toCollection(ArrayList<StudentGrades>::new));
    }
    System.out.println("DAO executed");
    return result;
  }
}
