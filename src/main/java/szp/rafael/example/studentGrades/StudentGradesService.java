package szp.rafael.example.studentGrades;

import io.quarkus.mongodb.panache.PanacheQuery;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Singleton
public class StudentGradesService {

  @Inject
  StudentGradesMockDAO dao;
  Logger logger = Logger.getLogger(this.getClass().getName());

  public List<StudentGrades> findAll() {
    PanacheQuery<StudentGradesCache> all = StudentGradesCache.findAll();
    ArrayList<StudentGrades> result = all.stream()
      .map(s -> s.studentGrades)
      .collect(Collectors.toCollection(ArrayList<StudentGrades>::new));
    return result;
  }

  public List<StudentGrades> findByName(String name){
    Map<String,Object> params = new HashMap<>();
    params.put("name","^.*?"+name+".*?$"); //regex
    List<StudentGrades> cacheFound = StudentGradesCache.find("studentGrades.studentName like :name", params)
      .list().stream()
      .map(s-> ((StudentGradesCache)s).studentGrades)
      .collect(Collectors.toCollection(ArrayList<StudentGrades>::new));

    if(cacheFound.size()>0){
      logger.info("Record found in cache");
      return cacheFound;
    }
    logger.info("Record not found in cache, trying DAO");
    List<StudentGrades> daoResult = dao.findByName(name);
    //UPDATING CACHE
    if(!daoResult.isEmpty()){
      daoResult.forEach(s-> {
        StudentGradesCache sgc = new StudentGradesCache();
        sgc.studentGrades=s;
        sgc.studentId=s.getId();
        sgc.lastUpdate= LocalDateTime.now();
        StudentGradesCache.persistOrUpdate(sgc);
      });
    }
    return daoResult;
  }

}
