package szp.rafael.example.studentGrades;

import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@MongoEntity(collection = "student_grades")
public class StudentGradesCache extends PanacheMongoEntityBase {


  @BsonId
  public Long studentId;
  public StudentGrades studentGrades;

  @BsonIgnore
  static Logger logger = Logger.getLogger(StudentGradesCache.class.getName());


  public LocalDateTime lastUpdate;


  public static void createTTLIndex(Long ttlSeconds){
    logger.info("Creating TTL Indexes");
    boolean found=false;
    for (Document index : mongoCollection().listIndexes()) {
      if(index.toJson().contains("lastUpdate")){
        found=true;
        logger.info("lastUpdate index found. Skipping creation");
        break;
      }
    }

    if(!found){
      logger.info("lastUpdate index not found. Creating a new one");
      IndexOptions indexOptions = new IndexOptions();
      indexOptions.expireAfter(ttlSeconds, TimeUnit.SECONDS);
      mongoCollection().createIndex(Indexes.ascending("lastUpdate"),indexOptions);
    }
  }

  public static void warmCache(StudentGradesMockDAO dao){
    List<StudentGrades> sgs = dao.findAll();
    sgs.stream().forEach(sg -> {
      StudentGradesCache sgc = new StudentGradesCache();
      sgc.studentId = sg.getId();
      sgc.lastUpdate = LocalDateTime.now();
      sgc.studentGrades = sg;
      StudentGradesCache.persistOrUpdate(sgc);
    });
  }

}
