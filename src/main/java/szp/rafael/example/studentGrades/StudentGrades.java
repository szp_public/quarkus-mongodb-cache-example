package szp.rafael.example.studentGrades;

import java.io.Serializable;
import java.util.Objects;

public class StudentGrades implements Serializable {

  public String studentName;
  public Long id;
  public float MathGrade;
  public float PortugueseGrade;
  public float NatureScienceGrade;

  public StudentGrades() {
  }

  public StudentGrades(String studentName, Long id, float mathGrade, float portugueseGrade, float natureScienceGrade) {
    this.studentName = studentName;
    this.id = id;
    MathGrade = mathGrade;
    PortugueseGrade = portugueseGrade;
    NatureScienceGrade = natureScienceGrade;
  }

  public String getStudentName() {
    return studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public float getMathGrade() {
    return MathGrade;
  }

  public void setMathGrade(float mathGrade) {
    MathGrade = mathGrade;
  }

  public float getPortugueseGrade() {
    return PortugueseGrade;
  }

  public void setPortugueseGrade(float portugueseGrade) {
    PortugueseGrade = portugueseGrade;
  }

  public float getNatureScienceGrade() {
    return NatureScienceGrade;
  }

  public void setNatureScienceGrade(float natureScienceGrade) {
    NatureScienceGrade = natureScienceGrade;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof StudentGrades)) return false;
    StudentGrades that = (StudentGrades) o;
    return getStudentName().equals(that.getStudentName()) && getId().equals(that.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getStudentName(), getId());
  }
}
