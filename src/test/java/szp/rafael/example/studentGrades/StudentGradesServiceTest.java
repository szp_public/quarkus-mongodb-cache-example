package szp.rafael.example.studentGrades;

import io.quarkus.test.junit.QuarkusTest;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
public class StudentGradesServiceTest {

  @Inject
  StudentGradesService service;

  @Test
  public void should_find_all_from_cache(){
    List<StudentGrades> all = service.findAll();
    assertTrue(all.size()>0);
  }

  @Test
  public void should_bypass_cache(){
    List<StudentGrades> found = service.findByName("BYPASS DAO");
    assertEquals(true,!found.isEmpty());
    StudentGradesCache.delete("_id",999_999_999L);
  }
}
