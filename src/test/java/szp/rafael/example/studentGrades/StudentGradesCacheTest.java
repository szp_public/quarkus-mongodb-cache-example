package szp.rafael.example.studentGrades;

import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
import io.quarkus.test.junit.QuarkusTest;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@QuarkusTest
public class StudentGradesCacheTest {

  @Inject
  StudentGradesMockDAO dao;

  @BsonIgnore
  @ConfigProperty(name = "example.cache.student-grades.ttl")
  public String ttlSeconds;


  @Test
  public void should_query_cache(){
    StudentGradesCache.findAll();
  }

  @Test
  public void should_init_cache_indexes(){
    StudentGradesCache.createTTLIndex(Long.valueOf(ttlSeconds));
  }

  @Test
  public void should_warm_cache(){
    StudentGradesCache.warmCache(dao);
  }

  @Test
  public void should_search_in_cache(){
    List<PanacheMongoEntityBase> sgs = StudentGradesCache.list("studentGrades.studentName", "SHARP CONTRACTION");
    System.out.println(sgs.size());
  }

  @Test
  public void should_search_with_find_and_like(){
    Map<String,Object> params = new HashMap<>();
    params.put("name","^.*?WHEAT$"); //regex
    List<PanacheMongoEntityBase> sgs = StudentGradesCache.find("studentGrades.studentName like :name", params).list();
    System.out.println(sgs.size());
  }

}
