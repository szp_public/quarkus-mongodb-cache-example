
DOCKER_COMPOSE=docker-compose -f ${PWD}/src/main/docker/mongodb/docker-compose.yml

mongodb.up:
	${DOCKER_COMPOSE} up -d

mongodb.down:
	${DOCKER_COMPOSE} down -v --remove-orphans