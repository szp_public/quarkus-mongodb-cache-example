# quarkus-mongodb-cache-example project

Example project which uses MOngoDB as cache (Panache)

### Frameworks used

1. Quarkus
1. Quarkus MongoDB-Client
1. Quarkus Panache MongoDB


### Requirements

You must have a MongoDB instance running and listening on  localhost:27017 socket

### Makefile and Mongo

There is a `Makefile` with some recipes/targets so you can start and stop your mongo instance properly. Alongside this tasks, there is also a `localhost:10000` mongo express server so you can manage to your localhost mongodb database.

If you are using Windows, you can copy paste the `mongodb.up` command with a subtle change in the volume mapping.


### Usage

All examples are in the [./src/tests/java](./src/tests/java) folder. Take an special look into `StudentGradeCache` and `StudentGradeService` classes.


### Additional Info

There is a `CacheWarmer` class that's initialized eagerly by quarkus. This class do 2 basic operations:
1. Create a TTL index so cache item can be invalidated according to the TTL set in `application.properties` file;
1. Warm cache with `example.mock.size` documents. This number can be changed in `application.properties` file.


If you stop your mongodb docker instance, the data will be erased. If you want your data to be persistent, tweat the [src/main/docker/mongodb/docker-compose.yml](./src/main/docker/mongodb/docker-compose.yml)

### TODO

1. The CacheWarmer must be improved so more caches can be initialized dynamically. The purpose of this project is just for the sake of demonstration, that's why I didn't create the related functionality. 

